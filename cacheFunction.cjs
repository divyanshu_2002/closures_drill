function cacheFunction(callBack) {
    const cache = {};
    if (typeof callBack == "function") {
        function cacheCalculate(element) {

            if (cache[element]) {
                console.log(`The value for this key already exists and the value is ${cache[element]} `);
                return cache[element];
            }
            else {
                const result = callBack(element);
                cache[element] = result;
                console.log(`value is stored in the cache for the key ${element} and the stored value is ${result}`)
                return result;
            }
        }
        return cacheCalculate;
    }
    else {
        console.log(`pass the method as callback`);
    }

}
module.exports = cacheFunction;