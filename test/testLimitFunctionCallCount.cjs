const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

function printTimes(count) {
    console.log(`I had called ${count} times`);
}

const limitToCall = 3;
const nofOfTimesToCall = 5;

try {
    const instanceOfLimitFunctionCall = limitFunctionCallCount(printTimes, limitToCall);

    for (let count = 1; count <= nofOfTimesToCall; count++) {
        const callBackMethod = instanceOfLimitFunctionCall();

        if (callBackMethod) {
            callBackMethod(count);
        }
        else {
            console.log("Limit reached to call!!");
            break;

        }
    }
}
catch (error) {
    console.log(error.message);
}

