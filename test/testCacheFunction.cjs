const cacheFunction = require("../cacheFunction.cjs");

function callBack(element) {
    return element * 2;
}

try {
    const cacheCalculate = cacheFunction(callBack);
    cacheCalculate(3);
    cacheCalculate(5);
    cacheCalculate(3);
}
catch (error) {
    console.log(error.message);
}