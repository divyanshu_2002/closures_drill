function counterFactory() {
    let counter = 0;

    function increment() {
        counter++;
        return counter;
    }

    function decrement() {
        counter--;
        return counter;
    }

    return { increment, decrement };
}

module.exports = counterFactory;