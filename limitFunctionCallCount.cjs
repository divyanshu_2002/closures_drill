function limitFunctionCallCount(callback, totalCount) {
    if(typeof callback=="function" && typeof totalCount=="number"){
        let count = 0;
        function printTimes() {
    
            if (totalCount > count) {
                count++;
                return callback;
            }
            else {
                return null;
            }
        };
        return printTimes;
    }
    else{
        console.log(`you must have to pass the callback as a function and totalcount as a number`);
    }
}
module.exports = limitFunctionCallCount;